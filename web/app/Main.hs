{-# LANGUAGE OverloadedStrings #-}

module Main where

import qualified HG.Controllers.Register
import qualified System.Environment
import qualified Web.Scotty

getPort :: IO Int
getPort = do
  port <- System.Environment.getEnv "PORT" 
  pure $ read port
  
main :: IO ()
main = do
  port <- getPort
  Web.Scotty.scotty port $ do
    Web.Scotty.get "/" $ do
      Web.Scotty.html "<h1>Hello, world</h1>"
    Web.Scotty.post "/register" $ 
      HG.Controllers.Register.registerAction
