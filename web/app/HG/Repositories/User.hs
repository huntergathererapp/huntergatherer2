{-# LANGUAGE OverloadedStrings #-}

module HG.Repositories.User where

import Database.PostgreSQL.Simple

register :: Database.PostgreSQL.Simple.Connection -> String -> String
  -> IO String
register connection username password = do
  [ Only token ] <- query connection "select auth.register(?, ?)" (username, password)
  return token
