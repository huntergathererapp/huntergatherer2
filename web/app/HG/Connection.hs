module HG.Connection where

import Database.PostgreSQL.Simple
--import Database.PostgreSQL.Simple.FromRow
--import qualified Database.PostgreSQL.Simple.ToField
--import qualified Database.PostgreSQL.Simple.ToRow
import qualified System.Environment

createConnectionInfo :: String -> String -> String -> String
  -> Database.PostgreSQL.Simple.ConnectInfo
createConnectionInfo database host password user =
  Database.PostgreSQL.Simple.defaultConnectInfo
  { Database.PostgreSQL.Simple.connectHost = host
  , Database.PostgreSQL.Simple.connectDatabase = database
  , Database.PostgreSQL.Simple.connectUser = user
  , Database.PostgreSQL.Simple.connectPassword = password
  }

getConnection :: IO Database.PostgreSQL.Simple.Connection
getConnection = do
  connectionInfo <- getConnectionInfo
  connect connectionInfo

getConnectionInfo :: IO Database.PostgreSQL.Simple.ConnectInfo
getConnectionInfo = do
  host <- System.Environment.getEnv "POSTGRES_HOST"
  database <- System.Environment.getEnv "POSTGRES_DB"
  user <- System.Environment.getEnv "POSTGRES_USER"
  password <- System.Environment.getEnv "POSTGRES_PASSWORD"
  pure (createConnectionInfo database host password user)
