{-# LANGUAGE OverloadedStrings #-}

module HG.Controllers.Register where

import Control.Monad.IO.Class
import Data.Aeson
import qualified HG.Connection
import qualified HG.Repositories.User
import qualified Web.Scotty

data Token = Token String

instance ToJSON Token where
  toJSON (Token token) =
    object [ "token" .= token ]

registerAction :: Web.Scotty.ActionM ()
registerAction = do
  username <- Web.Scotty.param "username" :: Web.Scotty.ActionM String
  password <- Web.Scotty.param "password" :: Web.Scotty.ActionM String
  connection <- liftIO $ HG.Connection.getConnection
  token <- liftIO $ HG.Repositories.User.register connection username password
  Web.Scotty.json $ Token token

