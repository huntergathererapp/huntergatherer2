#!/usr/bin/env nix-shell
#! nix-shell -i bash --pure
#! nix-shell -p bash cabal-install ghc postgresql zlib

cabal update
cabal install --installdir="./bin" --overwrite-policy=always
